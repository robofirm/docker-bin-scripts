#!/bin/bash
# Parse/validate arguments
options=$(getopt -o c:,u: --long container:,user: -- "$@")
[ $? -eq 0 ] || {
    echo "Incorrect options provided"
    exit 1
}
eval set -- "$options"
while true; do
    case "$1" in
    -c|--container)
        shift;
        CONTAINER="$1"
        ;;
    -u|--user)
        shift;
        CMD_USER="$1"
        ;;
    --)
        shift
        break
        ;;
    esac
    shift
done


export $(grep -v '^#' .env | xargs -d '\n')
if [[ -z "$CONTAINER" ]]; then
    if [[ -z "$CONDUCTOR_PHP_IMAGE" ]]; then
        CONTAINER="php-fpm"
    else
        CONTAINER="conductor"
    fi
fi

if [[ -z "$CMD_USER" ]]; then
    if [[ "$CONTAINER" == "php-fpm" || "$CONTAINER" == "conductor" ]]; then
        CMD_USER="webuser"
    else
        CMD_USER="root"
    fi
fi

CONTAINER="${CLIENT_NAME}-${CONTAINER}"

echo "Connecting to $CONTAINER bash prompt..."

# Run command
docker exec -u "$CMD_USER" -it "$CONTAINER" /bin/bash

